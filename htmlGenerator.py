# -*- coding: latin2 -*-

import xml.etree.ElementTree as ElementTree
import sys

'''
to use this class you must have some files in this folder:
- template-begin.html
- template-end.html
- bootstrap.min.css
- bootstrap.min.js

'''


class Generate:
    DESCRIPTION_TAG_NAME = 'description'
    def __init__(self, inFile, outFile):
        tree = ElementTree.parse(inFile)
        html = self.generateHtml(tree)
        with open (outFile, "w") as outputFile:
            outputFile.write(html.encode('utf8', 'xmlcharrefreplace'))

    def generateHtml(self, treeRoot):
        '''
        method builds html document based on whole XML tree and returns it as a string
        '''

        with open ("template-begin.html", "r") as template:
            html = template.read()

        for chunk in treeRoot.findall('chunk'):
            for sentence in chunk.findall('sentence'):
                for tok in sentence.findall('*'):
                    if tok.tag == 'ns':
                        if html[-1] == ' ':
                            html = html[:-1]
                    else:
                        description = tok.find(self.DESCRIPTION_TAG_NAME)
                        base = tok.find('lex').find('base').text
                        word = tok.find('orth').text
                        if description.text != None:
                            description.text = description.text.replace('"','&quot;')
                            '''
                            building link with a popover based on bootstrap.js library
                            '''
                            html += '<a class="clickable" data-toggle="popover" role="button" tabindex data-trigger="focus"\
                                    data-placement="bottom" title="' + base + '" data-content="' + description.text + '">' + word + '</a> '
                        else:
                            html += word + ' '
            html += '<br/>'

        with open ("template-end.html", "r") as template:
            html += template.read()

        return html
