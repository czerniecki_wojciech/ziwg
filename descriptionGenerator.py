# -*- coding: latin2 -*-

from xml.etree.ElementTree import SubElement, parse
import wikipedia

#for getting image size
import urllib
import ImageFile

import unicodedata

class NoOptionExist(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class Generate:
    DESCRIPTION_TAG_NAME = 'description'
    imgId = 0
    def __init__(self, inFile, outFile):
        wikipedia.set_lang('pl')
        tree = parse(inFile)
        self.parseXml(tree.getroot())
        tree.write(outFile)

    last_tok = None
    parent_node = None


    #function that put together tokens that are in one expression
    def addToPreviousNode(self, tok):
        self.last_tok.find('orth').text += " " + tok.find('orth').text
        self.last_tok.find('lex').find('base').text += " " + tok.find('lex').find('base').text

        
        #find ann with non-zero number inside and apply it to old tok
        for ann in tok.findall('ann'):
            if ann.text != "0":
                for lann in self.last_tok.findall('ann'):
                    if lann.get('chan') == ann.get('chan') and ann.text == lann.text:
                        lann.text = ann.text
        
    #function that create list of possible descriptions and calling apropriate function 
    #for each annotation.

    def addDescriptionToPreviousNode(self):
        if self.last_tok != None:
            categoriesFunctionsDict = {
                'nam_liv': self.personDescription,
                'nam_fac': self.roadDescription,
                'nam_loc': self.localizationDescription,
		        'nam_adj': self.adjectiveDescription,
                'nam_org': self.organizationDescription,
		        'nam_pro': self.proDescription,
                'nam_oth': self.othDescription,
                'nam_eve': self.eveDescription,
                'nam_num': self.numDescription
            }
            description = SubElement(self.last_tok, self.DESCRIPTION_TAG_NAME)

            for ann in self.last_tok.findall('ann'):
                if ann.text != '0':
                    description.text = categoriesFunctionsDict[ann.get('chan')](self.last_tok)
                    break
            description.tail = "\n   "

    def setPreviousNode(self, tok):
        self.last_tok = tok

    def isCategoryTheSameAsPrevious(self, tok):
        if self.last_tok != None:
            for ann in tok.findall('ann'):
                if ann.text != "0":
                    for lann in self.last_tok.findall('ann'):
                        if lann.get('chan') == ann.get('chan') and ann.text == lann.text:
                            return True
        return False

    #main function for parsing xml. Get tokens and call functions to produce descriptions 
    def parseXml(self, treeRoot):
        for chunks in treeRoot.findall('chunk'):
            for sentence in chunks.findall('sentence'):
                toks = sentence.findall('tok')
                tok_id = 0
                tok = None
                while tok_id < len(toks):
                    tok = toks[tok_id]

                    if self.isCategoryTheSameAsPrevious(tok):
                        self.addToPreviousNode(tok)
                        sentence.remove(tok)
                    else:
                        self.addDescriptionToPreviousNode()
                        self.setPreviousNode(tok)

                    tok_id += 1

                self.addDescriptionToPreviousNode()
                self.setPreviousNode(tok)

    def obtainSerchPhraseFromToken(self, tok):
        return tok.find('lex').find('base').text

    #next three functions search if any option has any existing page or summary.
    def tryGetWikipediaPage(self, options):
        for option in options:
            try:
                return wikipedia.page(option)
            except:
                pass
        raise NoOptionExist(len(detail.options))

    def tryGetWikipediaUrl(self, options):
        for option in options:
            try:
                return wikipedia.page(option).url
            except:
                pass
        raise NoOptionExist(len(detail.options))

    def tryGetWikipediaSummary(self, options):
        for option in options:
            try:
                return wikipedia.summary(option)
            except:
                pass
        raise NoOptionExist(len(detail.options))
        

    #Every function that use wikipedia.page or wikipedia.summary need to be in block try/except,
    #because page can be not created yet or can has ambigous anserws
    def generateWikipediaLink(self, tok):
        phrase = ""
        try:
            phrase = wikipedia.page(self.obtainSerchPhraseFromToken(tok)).url
        except wikipedia.exceptions.PageError as detail:
            print "Page error - to less information"
            phrase = "http://pl.wikipedia.org/wiki/" + self.obtainSerchPhraseFromToken(tok)
        except wikipedia.exceptions.DisambiguationError as detail:
            print "Disambiguation error choosing first option"
            try:
                phrase = self.tryGetWikipediaUrl(detail.options) #option can raise Page Error (E.g Kaczynski)
            except NoOptionExist:
                print "No option available"
                phrase = "http://pl.wikipedia.org/wiki/" + self.obtainSerchPhraseFromToken(tok) 
                # if there is no matching wiki page, default link is created,
                # which is possible to be created in future
        return "<a href ='"+ phrase + "'>" + phrase + "</a>"

    def getImageUrlFromWikipedia(self, tok):
        try:
            page = wikipedia.page(self.obtainSerchPhraseFromToken(tok))
        except wikipedia.exceptions.PageError as detail:
            print "Page error - to less information"
            return ""
        except wikipedia.exceptions.DisambiguationError as detail:
            print "Disambiguation error choosing first option"
            try:
                page = self.tryGetWikipediaPage(detail.options) 
            except NoOptionExist:
                print "No option available"
                return ""
        current_image_id = 0

        while current_image_id < len(page.images):
            url = page.images[current_image_id]
            if url.find(".jpg") > 0 or url.find(".jpeg") > 0:
                return url

            current_image_id += 1
        return False

    def getImageSize(self, uri):
        # get file size *and* image size (None if not known)
        file = urllib.urlopen(uri)
        size = file.headers.get("content-length")
        if size: size = int(size)
        p = ImageFile.Parser()
        while 1:
            data = file.read(1024)
            if not data:
                break
            p.feed(data)
            if p.image:
                return p.image.size
        file.close()
        return None

    def createImageHtmlContainer(self, imgUrl):
        if imgUrl:
            width, height = self.getImageSize(imgUrl)
            proportion = 240.0 / width

            width = str(round(width * proportion))
            height = str(round(height * proportion))

            id = str(self.imgId)
            self.imgId += 1

            return "<img src='" + imgUrl + "' id='img" + id + "' width=" + width + " height=" + height + ">"
        return ""

    def getWikipediaFirstParagraph(self, tok):
        result = ""
        try:
            result = wikipedia.summary(self.obtainSerchPhraseFromToken(tok))
        except wikipedia.exceptions.PageError as detail:
            print "Page error - to less information"
        except wikipedia.exceptions.DisambiguationError as detail:
            print "Disambiguation error choosing first option"
            try:
                result = self.tryGetWikipediaSummary(detail.options)
            except NoOptionExist:
                print "No option available"
        return result

    def getWikipediaDesription(self, tok):
        imgUrl = self.getImageUrlFromWikipedia(tok)
        return self.createImageHtmlContainer(imgUrl) + self.getWikipediaFirstParagraph(tok) + " <br/>Link do strony na wikipedii: " + self.generateWikipediaLink(tok)


    def generateDefaultMapView(self, tok):
        placeFullName = tok.find('lex').find('base').text + " "
        googleMapsLink = "<iframe width='240' height='180' frameborder='0' style='border:0' \
                src='https://www.google.com/maps/embed/v1/search?q=\
                "+placeFullName+"&key=AIzaSyCdWmVXdMXDCUX5MUJ2amO9e_HfHy26Vzo'></iframe>"
        return placeFullName+' na mapie: \n'+googleMapsLink

    def personDescription(self, tok):
        return self.getWikipediaDesription(tok)

    def roadDescription(self, tok):
        return self.generateDefaultMapView(tok)

    def localizationDescription(self, tok):
        description = self.generateDefaultMapView(tok) + "<br/>"
        description += self.getWikipediaFirstParagraph(tok)
        description += "<br/>Link do opisu lokalizacji w Wikipedii:\n"
        description += self.generateWikipediaLink(tok)
        return description

    def organizationDescription(self, tok):
        return self.getWikipediaDesription(tok)

    def adjectiveDescription(self, tok):
        return self.getWikipediaDesription(tok)

    def proDescription(self, tok):
        return self.getWikipediaDesription(tok)

    def othDescription(self, tok):
        return self.getWikipediaDesription(tok)

    def eveDescription(self, tok):
        return self.getWikipediaDesription(tok)

    def numDescription(self, tok):
        return self.getWikipediaDesription(tok)


