import sys
import descriptionGenerator
import htmlGenerator

#output name files are default, input xml name can be passed as a parameter
try: 
	inputCcl = sys.argv[1];
except:
	inputCcl = "in.xml";
outputCcl = "out.xml"
outputHtml = "out.html"

descriptionGenerator.Generate(inputCcl, outputCcl)
htmlGenerator.Generate(outputCcl, outputHtml)
